import React from 'react';
import GameInProgress from "./views/GameInProgress";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <div className="header">
          <Link to="/" className="mr-3">
            In progress
          </Link>
          <Link to="/result">
            Result
          </Link>
        </div>
        <Switch>
          <Route exact path="/">
            <h1>In progress</h1>
            <GameInProgress/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

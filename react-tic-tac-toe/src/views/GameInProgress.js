import React from 'react';

function Square (props) {
  return (
    <button
      className={`square ${props.isWon && 'winner-square'}`}
      onClick={props.onClick}
    >
      {props.value}
    </button>
  )
}

function Board(props) {
  return (
    [[0, 1, 2], [3, 4, 5], [6, 7, 8]].map(row =>
      <div className="board-row" key={row}>
        {
          row.map(cell => (
            <Square
              key={cell}
              value={props.squares[cell]}
              onClick={() => props.onClick(cell)}
              isWon={props.winnerLines.includes(cell)}
            />
          ))
        }
      </div>
    )
  );
}

export default class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        position: null,
        order: null
      }],
      xIsNext: true,
      stepNumber: 0,
      sortAscending: true
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares).winner || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares,
        position: i,
        order: history.length
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    })
  }

  handleSort(value) {
    this.setState({
      sortAscending: value
    })
  }

  render() {
    const { history } = this.state;
    const current = history[this.state.stepNumber];
    const { winner, winnerLines } = calculateWinner(current.squares);
    console.log('winnerLines', winnerLines);

    let moves = history
      .slice(1)
      .sort((a, b) => this.state.sortAscending ? a.order - b.order : b.order - a.order)
      .map((step, move) => {
        const movePosition = `${Math.floor(step.position / 3)} ${step.position % 3}`;
        const desc = 'Go to the move #' + move;
        return (
          <li key={step.order}>
            <button
              onClick={() => this.jumpTo(step.order)}
              className={step.order === this.state.stepNumber && 'selected-move'}
            >
              {desc} position: {movePosition}
            </button>
          </li>
        );
    });

    let status;
    if (winner) {
      status = winner + ' won'
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)}
            winnerLines={winnerLines}
          />
        </div>
        <div className="game-info">
          <div className="d-flex">
            <div className="mr-3">{status}</div>
            <button className="mr-3" onClick={e => this.handleSort(true, e)}>sort ascending</button>
            <button className="mr-3" onClick={e => this.handleSort(false, e)}>sort descending</button>
          </div>
          <button
            onClick={() => this.jumpTo(0)}
          >
            To start of the game
          </button>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  const result = {
    winner: null,
    winnerLines: []
  };
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      result.winner = squares[a];
      result.winnerLines = [a, b, c];
    }
  }
  return result;
}
